Changelog
---------

1.0rc2 (unreleased)
^^^^^^^^^^^^^^^^^^^

- Add Collection content type in configlet vocabulary for html_source [lepri]
- Add static resource to fix JS and CSS error [lepri]
- Fix JS variables in newsticker_js template [lepri]
- Tests updated [lepri]


1.0rc1 (2012-04-24)
^^^^^^^^^^^^^^^^^^^

- Initial release.

